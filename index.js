//console.log("Hi!");

/*
	Selection Control Structures
		- sorts out whether the statement/s are to be executed based on the condition whether it is true or false
	

	if else statement
	switch statement


	if.. else statement

	Syntax:
		if(condition) {
			//statement
		} else {
			//statement
		}

*/

// IF STATEMENT
	//executes a statement if a specified condition is true
	//can stand alone even without the else statement
	/*
		syntax:
			if(condition) {
				code block
			}

	*/

let numA = -1;

if (numA < 0) {
	console.log("Hello!");
}

console.log(numA < 0);

if(numA > 0) {
	console.log("This statement will not be printed!");
}

console.log(numA > 0);


//Another Example
let city = "New York";

if (city === "New York") {
	console.log("Welcome to New York City!");
}


//Else if
/*
	Executes a statement if previous conditions are false and if the specified condition is true
	Else if class is OPTIONAL and can be added to capture additional conditions to change the flow of a program

*/

let numB = 1;

if(numA > 0) {
	console.log("Hello from elseif");
} else if(numB > 0) {
	console.log("World!");
}

//Another Example
city = "Tokyo";

if (city === "New York") {
	console.log("Welcome to New York City")
} else if(city === "Tokyo") {
	console.log("Welcome to Tokyo!");
}


//else statement
/*
	Executes a statement if all other conditions are false
	The "else" statement is optional and can be added to capture any other result to change the flow of a program

*/

if (numA > 0) {
	console.log("Hello");
} else if (numB === 0) {
	console.log("World");
} else {
	console.log("Again!");
}

//Another Example
// let age = 20;

// if(age <= 18) {
// 	console.log("Not Allowed to Drink!");
// } else {
// 	console.log("Matanda ka na, shot na!");
// }


if (numA === -1) {
	console.log("Hello");
} else if (numB === 1) {
	console.log("World");
} else {
	console.log("Again!");
}


if (numA === -1) {
	console.log("Hello IF");
} 

if (numB === 1) {
	console.log("World IF");
} 

/*
	Mini-Activity: 6:40PM
		Create a conditional statement that if height is below 150, display "Did not passed the minimum height requirement".
		If above 150, display "Passed the minimum height requirement".

		**Stretch Goal:
			Put it inside a function

*/

//Solution 1
let height = 160;

if(height < 150) {
	console.log("Did not passed the minimum height requirement!");
} else {
	console.log("Passed the minimum height requirement!");
}

//Solution 2
function heightRequirement(h) {
	if(h < 150) {
		console.log("Did not passed the minimum height requirement!");
	} else {
		console.log("Passed the minimum height requirement!");
	}
}

heightRequirement(140);
heightRequirement(175);


//if, else if and else statement with Function
/*
- Most of the times we would like to use if, else if and else statements with functions to control the flow of our application
    - By including them inside functions, we can decide when certain conditions will be checked instead of executing statements when the JavaScript loads
    - The "return" statement can be utilized with conditional statements in combination with functions to change values to be used for other features of our application


*/

let message = 'No Message.';
console.log(message);


function determineTyphoonIntensity(windSpeed) {

	if(windSpeed < 30) {
		return 'Not a typhoon yet.';
	} else if (windSpeed <= 61) {
		return 'Tropical depression detected.';
	} else if(windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical Storm detected'
	} else if(windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected.';
	} else {
		return 'Typhoon detected.';
	}

}



message = determineTyphoonIntensity(70);
console.log(message);

// let windSpeed = prompt("Enter the windSpeed:");
// message = determineTyphoonIntensity(windSpeed);
// console.log(message);

// message = determineTyphoonIntensity(prompt("Wind Speed:"));
// console.log(message);

/* 
    - console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code
*/
if (message === 'Tropical Storm detected') {
	console.warn(message);
}


//Truthy and Falsy
/*
	- In Javascript a "Truthy" value is a value that is considered true when encountered in a Boolean context
	- Values are considered true unless defined otherwise
	-Falsy values/exceptions for truthy
	- Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
	- Commonly used for single statement execution where the result consists of only one line of code
	- For multiple lines of code/code blocks, a function may be defined then used in a ternary operator

	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN
*/

//Truthy Examples
if (true) {
	console.log("Truthy!");
}

if(1) {
	console.log("Truthy!");
}

if([]) {
	console.log("Truthy!");
}

//Falsy Examples
if(false) {
	console.log("Falsy!");
}

if(0) {
	console.log("Falsy!");
}

if(undefined) {
	console.log("Falsy!");
}

//Conditional (Ternary) Operator
	/*
		The condition(ternary) operator takes in three operands:
		1. condition
		2. expression to execute if the condition is truthy
		3. expression to execute if the condition is falsy

	-Can be used as an alternative to an "if else" statement
	- Ternary Operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be storred in a variable

	Syntax:
		(expression) ? ifTrue : ifFalse;

	*/

//Single Statement Execution
let ternaryResult = (1 < 18) ? "Statement is True" : "Statement is False";
console.log("Result of the Ternary Operator: " + ternaryResult);

//Multiple Statement Execution
// let name = prompt("Enter your name: "); 

// function isOfLegalAge() {
// 	// name = 'John';
// 	return 'You are of the legal age limit!';
// }

// function isUnderAge() {
// 	// name = 'Jane';
// 	return 'You are under the age limit!';
// }

// //The "parseInt" function converts the input received into a number data type
// let age = parseInt(prompt("What is your age?"));
// console.log(age);

// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
// console.log("Result of the Ternary Operator in Functions: " + legalAge + ", " + name);

//Switch Statement
/*
	Can be used as an alternative to an if.. else statement where the data used in the condition is of an expected input
   - The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters
   - The "expression" is the information used to match the "value" provided in the switch cases
   - Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
   - Switch cases are considered as "loops" meaning it will compare the "expression" with each of the case "values" until a match is found
   - The "break" statement is used to terminate the current loop once a match has been found
   - Removing the "break" statement will have the switch statement compare the expression with the values of succeeding cases even if a match was found
	  


	Syntax:
		switch (expression) {
			case <value>: 
				statement;
				break;
			
			default:
				statement;
				break;
		}
	
*/

//
// let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day)

// switch(day) {
// 	case 'monday', 'lunes':
// 		console.log("The color of the day is red");
// 		break;
// 	case 'tuesday':
// 		console.log("The color of the day is orange");
// 		break;
// 	case 'wednesday':
// 		console.log("The color of the day is yellow");
// 		break;
// 	case 'thursday':
// 		console.log("The color of the day is green");
// 		break;
// 	case 'friday':
// 		console.log("The color of the day is blue");
// 		break;
// 	case 'saturday':
// 		console.log("The color of the day is indigo");
// 		break;
// 	case "sunday":
// 		console.log("The color of the day is violet");
// 		break;
// 	default:
// 		console.log("Please input a valid day!");
// 		break;
// }


//Try-Catch-Finally Statement
	/*
		try catch are commonly used for error handling
		- There are instances when the application returns an error/warning that is not necessarily an error in the context of our code
	    - These errors are a result of an attempt of the programming language to help developers in creating efficient code
	    - They are used to specify a response whenever an exception/error is received
	    - It is also useful for debugging code because of the "error" object that can be "caught" when using the try catch statement
	    - In most programming languages, an "error" object is used to provide detailed information about an error and which also provides access to functions that can be used to handle/resolve errors to create "exceptions" within our code
	    - The "finally" block is used to specify a response/action that is used to handle/resolve errors


	*/

function showIntensityAlert(windSpeed) {

	try {
		//attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));
	} 

	 // error/err are commonly used variable names used by developers for storing errors
	catch (err) {

		// Catch errors within 'try' statement
        // In this case the error is an unknown function 'alerat' which does not exist in Javascript
        // The "alert" function is used similarly to a prompt to alert the user

		console.warn(err);
	}

	finally {
		// Continue execution of code regardless of success and failure of code execution in the 'try' block to handle/resolve errors
		alert('Intensity updates will show new alert');
	}

}

showIntensityAlert(56)
